Leviathan Game Engine
=====================

### Build Status

Master:
[![CircleCI](https://circleci.com/bb/hhyyrylainen/leviathan/tree/master.svg?style=svg)](https://circleci.com/bb/hhyyrylainen/leviathan/tree/master)
Develop:
[![CircleCI](https://circleci.com/bb/hhyyrylainen/leviathan/tree/develop.svg?style=svg)](https://circleci.com/bb/hhyyrylainen/leviathan/tree/develop)

[Project Website](https://leviathanengine.com)

